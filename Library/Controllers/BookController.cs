﻿using Library.Domain;
using Library.Service;
using Library.Service.dto;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    [ApiController]
    public class BookController : ControllerBase
    {
        // private readonly BookService _bookService = new BookService();
        private string path = "https://www.googleapis.com/books/v1/volumes?q=";

        [HttpGet]
        [Route("[controller]")]
        public ActionResult Get()
        {
            return Ok();
        }
        
        [HttpGet]
        [Route("[controller]/{title}")]
        public ActionResult<Book> GetBookByTitle(string title)
        {
            return Ok(BookService.GetProductAsync(path + "intitle:" + title + "&maxResults=1"));
        }
        
        // [HttpGet]
        // [Route("[controller]/{author}")]
        // public ActionResult GetBookByAuthor(string author)
        // {
        //     return Ok(BookService.GetProductAsync(path + "inauthor:" + author + "&maxResults=1"));
        // }
    }
}