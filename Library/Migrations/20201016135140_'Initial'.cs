﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.Data.EntityFrameworkCore.Metadata;

namespace Library.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessInfo",
                columns: table => new
                {
                    Id_accessInfo = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: true),
                    Viewability = table.Column<string>(nullable: true),
                    Embeddable = table.Column<bool>(nullable: false),
                    PublicDomain = table.Column<bool>(nullable: false),
                    TextToSpeechPermission = table.Column<string>(nullable: true),
                    WebReaderLink = table.Column<string>(nullable: true),
                    AccessViewStatus = table.Column<string>(nullable: true),
                    QuoteSharingAllowed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessInfo", x => x.Id_accessInfo);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id_book = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Kind = table.Column<string>(nullable: true),
                    TotalItems = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id_book);
                });

            migrationBuilder.CreateTable(
                name: "ImageLinkses",
                columns: table => new
                {
                    Id_image = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    SmallThumbnail = table.Column<string>(nullable: true),
                    Thumbnail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageLinkses", x => x.Id_image);
                });

            migrationBuilder.CreateTable(
                name: "OfferListPrice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AmountInMicros = table.Column<long>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferListPrice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SaleInfoListPrice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<double>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleInfoListPrice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VolumeInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    Subtitle = table.Column<string>(nullable: true),
                    Authors = table.Column<string>(nullable: true),
                    Publisher = table.Column<string>(nullable: true),
                    PublishedDate = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    PageCount = table.Column<long>(nullable: false),
                    PrintType = table.Column<string>(nullable: true),
                    MaturityRating = table.Column<string>(nullable: true),
                    AllowAnonLogging = table.Column<bool>(nullable: false),
                    ContentVersion = table.Column<string>(nullable: true),
                    ImageLinksId_image = table.Column<int>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    PreviewLink = table.Column<string>(nullable: true),
                    InfoLink = table.Column<string>(nullable: true),
                    CanonicalVolumeLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VolumeInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VolumeInfo_ImageLinkses_ImageLinksId_image",
                        column: x => x.ImageLinksId_image,
                        principalTable: "ImageLinkses",
                        principalColumn: "Id_image",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SaleInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: true),
                    Saleability = table.Column<string>(nullable: true),
                    IsEbook = table.Column<bool>(nullable: false),
                    ListPriceId = table.Column<int>(nullable: true),
                    RetailPriceId = table.Column<int>(nullable: true),
                    BuyLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SaleInfo_SaleInfoListPrice_ListPriceId",
                        column: x => x.ListPriceId,
                        principalTable: "SaleInfoListPrice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SaleInfo_SaleInfoListPrice_RetailPriceId",
                        column: x => x.RetailPriceId,
                        principalTable: "SaleInfoListPrice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IndustryIdentifiers",
                columns: table => new
                {
                    Id_Indus = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(nullable: true),
                    Identifier = table.Column<string>(nullable: true),
                    VolumeInfoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndustryIdentifiers", x => x.Id_Indus);
                    table.ForeignKey(
                        name: "FK_IndustryIdentifiers_VolumeInfo_VolumeInfoId",
                        column: x => x.VolumeInfoId,
                        principalTable: "VolumeInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Item",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Kind = table.Column<string>(nullable: true),
                    Etag = table.Column<string>(nullable: true),
                    SelfLink = table.Column<string>(nullable: true),
                    VolumeInfoId = table.Column<int>(nullable: true),
                    SaleInfoId = table.Column<int>(nullable: true),
                    AccessInfoId_accessInfo = table.Column<int>(nullable: true),
                    BookId_book = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Item", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Item_AccessInfo_AccessInfoId_accessInfo",
                        column: x => x.AccessInfoId_accessInfo,
                        principalTable: "AccessInfo",
                        principalColumn: "Id_accessInfo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Item_Books_BookId_book",
                        column: x => x.BookId_book,
                        principalTable: "Books",
                        principalColumn: "Id_book",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Item_SaleInfo_SaleInfoId",
                        column: x => x.SaleInfoId,
                        principalTable: "SaleInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Item_VolumeInfo_VolumeInfoId",
                        column: x => x.VolumeInfoId,
                        principalTable: "VolumeInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Offer",
                columns: table => new
                {
                    Id_Offer = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    FinskyOfferType = table.Column<long>(nullable: false),
                    ListPriceId = table.Column<int>(nullable: true),
                    RetailPriceId = table.Column<int>(nullable: true),
                    Giftable = table.Column<bool>(nullable: false),
                    SaleInfoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offer", x => x.Id_Offer);
                    table.ForeignKey(
                        name: "FK_Offer_OfferListPrice_ListPriceId",
                        column: x => x.ListPriceId,
                        principalTable: "OfferListPrice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Offer_OfferListPrice_RetailPriceId",
                        column: x => x.RetailPriceId,
                        principalTable: "OfferListPrice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Offer_SaleInfo_SaleInfoId",
                        column: x => x.SaleInfoId,
                        principalTable: "SaleInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IndustryIdentifiers_VolumeInfoId",
                table: "IndustryIdentifiers",
                column: "VolumeInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Item_AccessInfoId_accessInfo",
                table: "Item",
                column: "AccessInfoId_accessInfo");

            migrationBuilder.CreateIndex(
                name: "IX_Item_BookId_book",
                table: "Item",
                column: "BookId_book");

            migrationBuilder.CreateIndex(
                name: "IX_Item_SaleInfoId",
                table: "Item",
                column: "SaleInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Item_VolumeInfoId",
                table: "Item",
                column: "VolumeInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_ListPriceId",
                table: "Offer",
                column: "ListPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_RetailPriceId",
                table: "Offer",
                column: "RetailPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Offer_SaleInfoId",
                table: "Offer",
                column: "SaleInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleInfo_ListPriceId",
                table: "SaleInfo",
                column: "ListPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleInfo_RetailPriceId",
                table: "SaleInfo",
                column: "RetailPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_VolumeInfo_ImageLinksId_image",
                table: "VolumeInfo",
                column: "ImageLinksId_image");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IndustryIdentifiers");

            migrationBuilder.DropTable(
                name: "Item");

            migrationBuilder.DropTable(
                name: "Offer");

            migrationBuilder.DropTable(
                name: "AccessInfo");

            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "VolumeInfo");

            migrationBuilder.DropTable(
                name: "OfferListPrice");

            migrationBuilder.DropTable(
                name: "SaleInfo");

            migrationBuilder.DropTable(
                name: "ImageLinkses");

            migrationBuilder.DropTable(
                name: "SaleInfoListPrice");
        }
    }
}
