﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using Library.Domain;

namespace Library.Service.dto
{
    public class BookDTO
    {
        public string Id_item, Title, Authors, Publisher, Description, PrintType, MaturityRating, Language, PreviewLink, InfoLink;
        // public IndustryIdentifier IndustryIdentifiers;
        public long PageCount;
        public DateTime PublishedDate;
        // public ImageLinks ImageLinks;

        public BookDTO(string id, string title, string authors, string publisher, DateTime publishedDate, string description, long pageCount, string printType, string maturityRating, string language, string previewLink, string infoLink)
        {
            Id_item = id;
            Title = title;
            Authors = authors; //A voir 
            Publisher = publisher;
            PublishedDate = publishedDate;
            Description = description;
            PageCount = pageCount;
            PrintType = printType;
            MaturityRating = maturityRating;
            Language = language;
            PreviewLink = previewLink;
            InfoLink = infoLink; //A voir
            // IndustryIdentifiers = industryIdentifiers; //A voir
            // ImageLinks = imageLinks; //A voir
        }
    }
}