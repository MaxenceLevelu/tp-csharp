﻿using System.Security.Policy;
using Library.Domain;
using Microsoft.EntityFrameworkCore;
using MySQL.Data.EntityFrameworkCore;

namespace Library.Service
{
    public class LibraryContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<IndustryIdentifier> IndustryIdentifiers { get; set; }
        public DbSet<ImageLinks> ImageLinkses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;database=books;user=root;password=root");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // base.OnModelCreating(modelBuilder);
            //
            // modelBuilder.Entity<Item>(entity =>
            // {
            //     entity.HasKey(e => e.Id);
            //     entity.Property(e => e.VolumeInfo.Title).IsRequired();
            //     entity.HasOne(d => d.Publisher)
            //         .WithMany(p => p.Books);
            // });
        }
    }
}