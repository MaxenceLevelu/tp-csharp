﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Library.Domain;
using Library.Service.dto;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Library.Service
{
    public class BookService
    {
        static HttpClient client = new HttpClient();
        
        public static LibraryContext db = new LibraryContext();
        
        // public static BookDTO GetBook(int id)
        // {
        //     
        // }
        
        public static async Task<Book> GetProductAsync(string path)
        {
            Book book = null;
            BookDTO dto = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                //var livre = await response.Content.ReadAsStringAsync();
                dynamic test = JsonConvert.DeserializeObject<Book>(await response.Content.ReadAsStringAsync());
                book = new Book()
                {
                    Kind = test.kind,
                    Items = new List<Item>(),
                    TotalItems = test.totalItems
                };
                foreach (var VARIABLE in test.items)
                {
                    Item item = new Item()
                    {
                        Id = VARIABLE.id,
                        
                    };
                }
                
                db.Books.Add(book);
                db.SaveChanges();
                // var item = book.Items[0];
                // var volInfo = item.VolumeInfo;
                // var indus = volInfo.IndustryIdentifiers[0];
                // dto = new BookDTO(item.Id, volInfo.Title, volInfo.Authors, volInfo.Publisher,
                //     volInfo.PublishedDate.DateTime, volInfo.Description, volInfo.PageCount,
                //     volInfo.PrintType, volInfo.MaturityRating, volInfo.Language, volInfo.PreviewLink.ToString(),
                //     volInfo.InfoLink.ToString());
            }
            return book;
        }
    }
}